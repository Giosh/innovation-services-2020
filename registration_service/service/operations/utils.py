import requests
import subprocess
import json
from werkzeug.exceptions import NotFound


class PhoneNumber():
    def __init__(self, number):
        self.phone_number = number

    def is_valid(self):
        if len(self.phone_number) == 10:
            return True
        else:
            return False

    def get_uid(self):
        req = requests.get(
              "http://profiles:80/userinfo/phone/{}".format(self.phone_number))
        if req.status_code == 200:
            return int(float(req.text.strip()))
        if req.status_code == 404:
            raise NotFound

class NewUser():
    def __init__(self, user_data):
        self.user = user_data
        self.status = {}
        self.status["code"] = 201
        self.status["profile_message"] = "not attempted"
        self.status["matches_message"] = "not attempted"
        self.user["uid"] = self.__new_uid()

    def register(self):
        self.__create_profile()
        self.__create_matches()
        self.status["uid"] = self.user['uid']
        

    def __create_profile(self):
        req = requests.put("http://profiles:80/create/", data = self.user)
        self.status["profile_message"] = req.json()
        self.status["profile"] = req.status_code

    def __create_matches(self):
        req = requests.put("http://matches:80/create/{}".format(
            self.user["uid"]))
        self.status["matches_message"] = req.json()
        self.status["matches"] = req.status_code


    def __new_uid(self):
        req = requests.get("http://profiles:80/users/")
        all_users = req.json()["Users"]
        return max(all_users) + 1
