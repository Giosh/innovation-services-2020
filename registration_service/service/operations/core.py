from flask_restful import Resource, reqparse
from flask import request
from werkzeug.exceptions import NotFound

from .utils import PhoneNumber, NewUser


class CheckPhone(Resource):
    def get(self, number):
        num = PhoneNumber(number)
        message = {}
        html_code = 200
        if not num.is_valid():
            return {"message": "Provided phone number not valid"}, 412

        try:
            message['uid'] = num.get_uid()
            message['message'] = "User Exists"
        except ConnectionError:
            html_code = 500
            message["message"] = "Failed to reach profiles service"
            message["uid"] = -1
        except ValueError:
            html_code = 500
            message["message"] = "Invalid ID returned"
            message["uid"] = -1
        except NotFound:
            html_code = 404
            message["message"] = "Phone number unknown"
            message["uid"] = -1
        except Exception as e:
            html_code = 500
            message["message"] = "unknown error: {}".format(e)
            message["uid"] = -1

        return message, html_code


class Register(Resource):
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('phone', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('birthday', type=str, required=True)
        parser.add_argument('bio', type=str, required=True)
        args = parser.parse_args(strict=True)

        user = NewUser(args)
        user.register()

        return user.status, user.status["code"]
