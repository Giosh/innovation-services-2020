from flask import Flask
from flask_restful import Api

from operations.core import CheckPhone, Register

app = Flask(__name__)
api = Api(app)

api.add_resource(CheckPhone, '/check_phone/<string:number>', methods=['GET'])
api.add_resource(Register, '/register/', methods=['POST'])

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=80, debug=True)