
Noelle Cicilia, 2048639
Lucas Viesel, 2048222
Mariagiorgia Agnese Tandoi, 2046991



# Mini Tinder
Mini Tinder is a minimalistic implemetation of the Tinder registration and 
swiping process. It is implemented with a Micro-Service architecture.

## Services
Bellow the implemented Services are presented.

### Registration Service
#### Description
The registration service governs the login and registration process on the user
side and ensures the creation of required data structures internally. It is
mapped to **port 10801**.

#### Public Endpoints
---
**/check_phone/\<string:number>**  
Validates a phone number and checks if the user is registered.  

Call
 - Method: GET
 - Body: 

Return
 - {"uid": \<uid>,
    "message": \<message>}

Errors:
 - Invalid phone number (412)
 - Phone number unknown (404)

---
**/register/**  
Registers a new user. (Creates relevant internal data structures)  

Call
 - Method: Post
 - Body: {"phone":\<phone>,
          "name":\<name>, 
          "birthday":\<birthday>, 
          "bio":\<bio>}

Return
 - {"uid": \<uid>, ...}

### Profiles Service
#### Description
This is a purely internal service, handling keeping track of the users and their 
profiles. It is mapped to **port 10802**  
#### Internal Endpoints
---
**/userinfo/phone/\<string:number>**  
Get the uid of a user based on their phone number  

Call
 - Method: GET
 - Body: -

Return
 - {"uid": \<uid>,
    "message": \<message>}

Errors:
 - Phone number unknown (404)
---
**/create/**  
Adds a user to the database  

Call
 - Method: PUT
 - Body: {"uid":\<uid>
          "phone":\<phone>,
          "name":\<name>, 
          "birthday":\<birthday>, 
          "bio":\<bio>}

Return
 - 201
---
**/users/**  
Retrieves a list of all users  

Call
 - Method: GET
 - Body: -

Return
 - {"users": [\<uid>,..., \<uid>]}

---
**/public_profile/\<int:uid>**  
Retrieves the public profile of a given user  

Call
 - Method: GET
 - Body: -

Return
 - {"uid":\<uid>
    "name":\<name>, 
    "bio":\<bio>}

### Matching Service
#### Description
Keeps track about relations between users. Mapped to **port 10803**.
#### Public Endpoints
---
**/swipe/\<string:response>**  
Indicate the response of one user to the other  

Call
 - Method: POST
 - Body: {"uid":\<uid>
          "target":\<uid>}
 - response in path is like or nope

Return
 - 201

#### Internal Endpoints
---
**/create/\<string:uid>**  
Creates the Relevant data structures for a new user  

Call
 - Method: PUT
 - Body: -

 Return:
  - 201

---
**/swiped/\<string:uid>**  
get the users to which a given user has responded  

Call
 - Method: GET
 - Body: -

 Return
  - {"like": [\<uid>,..., \<uid>], 
     "nope": [\<uid>,..., \<uid>]}

### Candidate Service
#### Description
This service selects the next profile for swiping. It is mapped to **port 10804**
#### Public Endpoints
---
**/next_candidate/\<int:uid>**  
Get the profile of the next candidate  

Call
 - Method: GET
 - Body: -

 Return:
 - {"uid":\<uid>
    "name":\<name>, 
    "bio":\<bio>}

Errors:
 - No 'unswiped' users available (404)

## Deployment
The services are deployed a docker containers manged with docker-compose. 
Internal communication runs via the standard network created by docker-compose

All services are interfaced via a production grade nginx server with Gunicorn gateway interface.

## Orchestration
Service Orchestration is implemented in two flavors. 
 - First as executable BPMN model
 - Second as interactive command-line interface