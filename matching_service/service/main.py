from flask import Flask
from flask_restful import Api

from operations.core import CreateUser, PriorResponses, Swipe

app = Flask(__name__)
api = Api(app)

api.add_resource(CreateUser, '/create/<string:uid>', methods=['PUT'])
api.add_resource(PriorResponses, '/swiped/<string:uid>', methods=['GET'])
api.add_resource(Swipe, '/swipe/<string:response>', methods=['POST'])

if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)