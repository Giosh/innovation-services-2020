from flask_restful import Resource, reqparse
from flask import request

from .data import MatchesDB
from .utils import Match

class CreateUser(Resource):
    def put(self, uid):
        try:
            MatchesDB().create(uid)
            return {"message": "User Added successfully"}, 201
        except Exception as e:
            return {"message": str(e)}, 500

class PriorResponses(Resource):
    def get(self, uid):
        return MatchesDB().select(uid, ["like", "nope"]), 200

class Swipe(Resource):
    def post(self, response):
        parser = reqparse.RequestParser()
        parser.add_argument('uid', type=int, required=True)
        parser.add_argument('target', type=int, required=True)
        args = parser.parse_args(strict=True)

        match = Match(**args)

        if response == "like":
            return match.like(), 201
        elif response == "nope":
            return match.nope(), 201
        else:
            return {"message": "Invalid response"}, 400
        