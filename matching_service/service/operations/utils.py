from .data import MatchesDB

class Match():
    def __init__(self, uid, target):
        self.__uid = uid
        self.__target = target
        self.__DB = MatchesDB()

    def like(self):
        if self.__is_a_match():
            message = {"message": "Match"}
        else:
            message = {"message": "Response recorded"}

        self.__DB.insert(self.__uid, "like", self.__target)
        self.__DB.insert(self.__target, "liked", self.__uid)

        return message

    def nope(self):
        self.__DB.insert(self.__uid, "nope", self.__target)
        self.__DB.insert(self.__target, "noped", self.__uid)

        return {"message": "Response recorded"}

    def __is_a_match(self):
        liked_by =  self.__DB.select(self.__uid, ["liked"])

        return (self.__target in liked_by["liked"])
