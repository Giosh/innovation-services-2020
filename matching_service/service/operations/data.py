import json

class MatchesDB:
    def __init__(self):
        with open('/data/matches.json') as f:
            self.__data = json.load(f)
    
    def create(self, uid):
        self.__data[uid] = {
                    "like": [],
                    "nope": [],
                    "liked": [],
                    "noped": []
                        }
        self.__store()

    def select(self, uid, fields=[]):
        if fields:
            return {key : self.__data[str(uid)][key]
                    for key in fields}
        else:
            return self.__data[uid]

    def insert(self, focus, filed, associate):
        self.__data[str(focus)][filed].append(associate)
        self.__store()

    def __store(self):
        with open('/data/matches.json', 'w') as f:
            json.dump(self.__data, f)

