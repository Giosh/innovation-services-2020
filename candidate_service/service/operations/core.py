from flask_restful import Resource

from .utils import NextUser

class Candidate(Resource):
    def get(self, uid):
        try:
            source = NextUser(uid)
            return source.next_candidate(), 200
        except:
            return {"uid": 0, "name":"", "bio":""}, 404
