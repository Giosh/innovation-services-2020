import requests
import random


class NextUser():
    def __init__(self, number):
        self.__source = number

    def __get_swiped_users(self):
        resp = requests.get("http://matches:80/swiped/{}".format(self.__source))
        combined = [x for result in resp.json().values() for x in result]
        return combined
        

    def __get_all_users(self):
        req = requests.get("http://profiles:80/users/")
        return req.json()["Users"]

    def __get_candidate_users(self):
        swiped = self.__get_swiped_users()
        return [x 
                for x in self.__get_all_users() 
                if x not in swiped and x != self.__source]

    def next_candidate(self):
        candidate_id = random.choice(self.__get_candidate_users())
        resp = requests.get(
            "http://profiles:80/public_profile/{}".format(candidate_id))
        
        return resp.json()