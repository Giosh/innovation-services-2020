from flask import Flask
from flask_restful import Api

from operations.core import Candidate

app = Flask(__name__)
api = Api(app)

api.add_resource(Candidate, '/next_candidate/<int:uid>', methods=['GET'])

if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)