import requests

SERVER = "http://localhost:"

def check_phone(number):
    """
    UID or 0 if user does not exist or -1 if the phone number is invalid
    """
    resp = requests.get("{}10801/check_phone/{}".format(SERVER, number))
    try:
        return resp.json()["uid"]
    except KeyError:
        if resp.json()["message"] == "Provided phone number not valid":
            return 0


def register(phone, name, birthday, bio):
    my_data = {"phone":phone, "name":name, "birthday":birthday, "bio":bio}
    resp = requests.post("{}10801/register/".format(SERVER), data=my_data)
    try:
        return resp.json()["uid"]
    except KeyError:
        return 0

def candidate(uid):
    resp = requests.get("{}10804/next_candidate/{}".format(SERVER, uid))
    if resp.status_code != 200:
        return {}
    else:
        return resp.json()

def nope(uid, target):
    my_data = {"uid": uid, "target": int(target)}
    requests.post("{}10803/swipe/nope".format(SERVER), data = my_data)

def like(uid, target):
    my_data = {"uid": uid, "target": int(target)}
    resp = requests.post("{}10803/swipe/like".format(SERVER), data = my_data)

    if resp.json()["message"] == "Match":
        return True
    else:
        return False