from prompt_toolkit.application import Application
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.layout.containers import HSplit, VSplit, Float, FloatContainer, Window
from prompt_toolkit.layout.controls import FormattedTextControl
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.widgets import Frame
from prompt_toolkit.layout import WindowAlign

from prompt_toolkit.styles import Style
from prompt_toolkit.widgets import Box, Button, Frame, Label, TextArea
from prompt_toolkit.key_binding.bindings.focus import (
    focus_next,
    focus_previous,
)
from prompt_toolkit.application.current import get_app

import api

class my_app:
    def __init__(self):

        self.__step = self.__login
        self.container = FloatContainer(
                                content=Window(style='class:background'),
                                floats=[],
                            )

        self.__buttons()
        self.__fields()

        self.focus = self.__phone_number_field

    def current_step(self):
        return self.__step()

    def __login(self):
        window = Float(
            Frame(HSplit([
                HSplit([
                    Window(FormattedTextControl("Enter Phonenumber:"), width=20, height=1),
                    Box(Frame(self.__phone_number_field), padding=1,)
                    ]),
                Box(self.__login_button, padding=1,)
            ]),
                style="bg:#ffffff #bd1767",
            )
        )
        return window

    def click_login(self):
        self.__phone_number = self.__phone_number_field.text
        self.__uid = api.check_phone(self.__phone_number)

        if self.__uid > 0:
            self.__step = self.__welcome
            self.render()
            get_app().layout.focus(self.__swipe_button)
        if self.__uid == -1:
            self.__step = self.__register
            self.render()
            get_app().layout.focus(self.__name_field)
        
        
    def __register(self):
        window = Float(Frame(HSplit([
                Window(FormattedTextControl("Please Enter \n your information!"),
                     width=45, height=3, align=WindowAlign.CENTER),
                VSplit([
                    Window(FormattedTextControl("\n  Name:  "),
                        align=WindowAlign.CENTER),
                    Box(Frame(self.__name_field), padding = 0)
                ]),
                VSplit([
                    Window(FormattedTextControl("\nBirthday:"),
                        align=WindowAlign.CENTER),
                    Box(Frame(self.__birthday_field), padding = 0)
                ]),
                VSplit([
                    Window(FormattedTextControl("\n  Bio:  "),
                        align=WindowAlign.CENTER),
                    Box(Frame(self.__bio_field), padding = 0)
                ]),
                Box(Frame(self.__register_button))
            ]), style="bg:#ffffff #000000"),
        )
        return window

    def click_register(self):
        self.__name = self.__name_field.text
        self.__birthday = self.__birthday_field.text
        self.__bio = self.__bio_field.text

        self.__uid = api.register(self.__phone_number, self.__name,
                                  self.__birthday, self.__bio)
        
        if self.__uid > 0:
            self.__step = self.__welcome
            self.render()
            get_app().layout.focus(self.__swipe_button)
        else:
            self.__phone_number = ""
            self.__uid = 0
            self.__step = self.__login
            self.render()


    def __welcome(self):
        window = Float(
            Frame(HSplit([
                Window(FormattedTextControl("Welcome!"), width=25, height=1),
                Box(Frame(self.__swipe_button), padding=1)
            ]),
                style="bg:#ffffff #bd1767",
            )
        )
        return window


    def click_swipe(self):
        self.__candidate = api.candidate(self.__uid)

        if self.__candidate:
            self.__step = self.__swipe
            self.render()
            get_app().layout.focus(self.__like_button)
        else:
            self.__step = self.__no_candidates_screen
            self.render()
            get_app().layout.focus(self.__swipe_button)

    def __swipe(self):
        window = Float(
            Frame(HSplit([
                Window(FormattedTextControl("Go on a Date?"), width=25, height=1,
                        align=WindowAlign.CENTER),
                Window(FormattedTextControl(self.__candidate["name"]), width=25, height=1,
                        align=WindowAlign.CENTER),
                Window(FormattedTextControl(self.__candidate["bio"]), width=25, height=1,
                        align=WindowAlign.CENTER),
                VSplit([
                    Box(Frame(self.__like_button, style="bg:#73e600 #336600"), padding=1),
                    Box(Frame(self.__nope_button, style="bg:#ff1a1a #990000"), padding=1),
                ])
            ]),
                style="bg:#ffffff #bd1767",
            )
        )
        return window


    def click_like(self):
        its_a_match = api.like(self.__uid , self.__candidate["uid"])
        
        if its_a_match:
            self.__step = self.__match_screen
            self.render()
            get_app().layout.focus(self.__swipe_button)
        else:
            self.__candidate = api.candidate(self.__uid)

            if self.__candidate:
                self.__step = self.__swipe
                self.render()
                get_app().layout.focus(self.__like_button)
            else:
                self.__step = self.__no_candidates_screen
                self.render()
                get_app().layout.focus(self.__swipe_button)

       
    def click_nope(self):
        api.nope(self.__uid , self.__candidate["uid"])
        self.__candidate = api.candidate(self.__uid)

        if self.__candidate:
            self.__step = self.__swipe
            self.render()
            get_app().layout.focus(self.__like_button)
        else:
            self.__step = self.__no_candidates_screen
            self.render()
            get_app().layout.focus(self.__swipe_button)

    def __match_screen(self):
        window = Float(
            Frame(HSplit([
                Window(FormattedTextControl("A Match!"), width=25, height=1),
                Box(Frame(self.__swipe_button), padding=1)
            ]),
                style="bg:#ffffff #bd1767",
            )
        )
        return window

    def __no_candidates_screen(self):
        window = Float(
            Frame(HSplit([
                Window(FormattedTextControl(
                    "There are no candidates.\nTry it later!"), width=25, height=1),
                Box(Frame(self.__swipe_button), padding=1)
            ]),
                style="bg:#ffffff #bd1767",
            )
        )
        return window

    def click_exit(self):
        get_app().exit()

    def click_logout(self):
        self.__phone_number = ""
        self.__uid = 0
        self.__step = self.__login
        self.render()

    def render(self):
        structure = [
        # background
        Float(
            Frame(
                Window(width=78, height=20),
                style="bg:#bd1767 #ffffff",
            ),
            bottom=3,
        ),
        # Bottom float.
        Float(
            VSplit([
                Box(Frame(self.__logout_button), padding=0),
                Window(FormattedTextControl(
                    "\nCommand line Tinder"), align=WindowAlign.CENTER),
                Box(Frame(self.__exit_button), padding=0),
                ], style="bg:#ffffff #000000"),
            width=80, height=3,
            bottom=0
        ),
        # Center float.
        my_application.current_step(),
        ]

        self.container.floats = structure

    def __buttons(self):
        self.__login_button = Button('Login', handler=self.click_login)
        self.__register_button = Button('Register', handler=self.click_register)
        self.__swipe_button = Button('Start Swiping', handler=self.click_swipe, 
                                     width=20)
        self.__logout_button = Button('Logout', handler=self.click_logout)
        self.__exit_button = Button('Exit', handler=self.click_exit)

        self.__like_button = Button('Like', handler=self.click_like)
        self.__nope_button = Button('Nope', handler=self.click_nope)


    def __fields(self):
        self.__phone_number_field = TextArea(focusable=True)
        self.__name_field = TextArea(focusable=True)
        self.__birthday_field = TextArea(focusable=True)
        self.__bio_field = TextArea(focusable=True)


my_application = my_app()
my_application.render()

# 2. Key bindings
kb = KeyBindings()
kb.add('tab')(focus_next)
kb.add('s-tab')(focus_previous)

@kb.add("q")
def _(event):
    " Quit application. "
    event.app.exit()


# 3. The `Application`
application = Application(layout=Layout(my_application.container, 
    focused_element=my_application.focus), key_bindings=kb,
    full_screen=True, mouse_support=True)

def run():
    application.run()


if __name__ == "__main__":
    run()