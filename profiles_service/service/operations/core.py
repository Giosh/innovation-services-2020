from flask_restful import Resource, reqparse
from flask import request
import pandas as pd
import json
from .utils import UserInfo


class UserPhoneInfo(Resource):

    def get(self, number):
        user_info = UserInfo()
        try:
            return user_info.get_user('phone', number)['uid'].values[0], 200
        except:
            return {"message": "Unknown phone number"}, 404


class CreateUser(Resource):
    def put(self):
        parser = reqparse.RequestParser()
        parser.add_argument('uid', type=int, required=True)
        parser.add_argument('phone', type=str, required=True)
        parser.add_argument('name', type=str, required=True)
        parser.add_argument('birthday', type=str, required=True)
        parser.add_argument('bio', type=str, required=True)
        args = parser.parse_args(strict=True)

        try:
            UserInfo().add_user(args)
            return {"message": "User Added successfully"}, 201
        except Exception as e:
            return {"message": str(e)}, 500

class GetUsers(Resource):
    def get(self):
        return {"Users": UserInfo().get_users()} , 200

class PublicProfile(Resource):
    def get(self, uid):
        return UserInfo().public_info(uid), 200


