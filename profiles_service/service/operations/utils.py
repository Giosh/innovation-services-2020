import pandas as pd

class UserInfo:
    def __init__(self):
        self.profile_df = pd.read_csv("/data/profiles.csv")

    def get_user(self, key_name, key_value):
        # making boolean series for the phone number
        filter = self.profile_df[key_name] == key_value

        return self.profile_df.where(filter).dropna()

    def get_next_uid(self):
        return max(self.profile_df.uid.values) + 1

    def add_user(self, args):
        self.profile_df = self.profile_df.append(args, ignore_index=True)
        self.profile_df.to_csv("/data/profiles.csv")
    
    def get_users(self):
        return [x for x in self.profile_df["uid"].tolist() if x != 0]

    def public_info(self, uid):
        all_info = self.get_user("uid", uid)
        public_keys = ["uid", "name", "bio"]
        return {key: all_info[key].values[0] for key in public_keys}