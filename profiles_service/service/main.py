from flask import Flask
from flask_restful import Api

from operations.core import UserPhoneInfo, CreateUser, GetUsers, PublicProfile

app = Flask(__name__)
api = Api(app)

api.add_resource(
    UserPhoneInfo, '/userinfo/phone/<string:number>', methods=['GET'])
api.add_resource(CreateUser, '/create/', methods=['PUT'])
api.add_resource(GetUsers, '/users/', methods=['GET'])
api.add_resource(PublicProfile, '/public_profile/<int:uid>', methods=['GET'])


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=80, debug=True)